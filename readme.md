Command-line tool to shorten URLs using the excellent [v.gd](https://v.gd) [URL shortener](https://en.wikipedia.org/wiki/URL_shortening).

This tool is [free software](https://en.wikipedia.org/w/index.php?title=Free_software&oldid=695656324#Definition_and_the_Four_Freedoms) under the [GNU Affero General Public License, version 3](https://www.gnu.org/licenses/agpl-3.0.html).

Assuming GNU/Linux with the `apt-get` command installed and working, GNU Bash as your shell, and installation rights available using `sudo`, you can install `vgd` as follows:

    sudo apt-get install curl gridsite-clients
    mkdir -p ~/bin
    curl 'https://gitlab.com/sampablokuper/vgd/raw/master/vgd' > ~/bin/vgd
    chmod +x ~/bin/vgd
    echo -e '\n# Add ~/bin to PATH\nexport PATH="$HOME/bin:$PATH"' >> ~/.bashrc
    source ~/.bashrc

You can now run vgd by entering `vgd` followed by the URL you want to shorten. Put the URL in single quotes. The tool will return a shortened URL. For example:

    $ vgd 'example.com'
    https://v.gd/RaZxUe

An example using a longer URL:

    $ vgd 'https://en.wikipedia.org/w/index.php?title=CURL&action=history'
    https://v.gd/35qysF

N.B. This tool uses the v.gd API, and is therefore subject to that API's [restrictions/limitations](https://v.gd/apishorteningreference.php#restrict).
